from django.http.response import HttpResponseRedirect
from django.shortcuts import render,redirect
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth import authenticate,login,logout
from products.models import productitems
from accounts.models import cartitem,carttotal,checkout,wishitems,vendorcheckout,ChangePassword
from django.contrib import messages
import yaml
from django.core.mail import EmailMessage
# Create your views here.
from django.contrib.auth.models import User

def sign_in_view(request):
    return render(request,'login-register.html')


def wishviewbasic(request):
    user_ins=request.user
    wishdetail=wishitems.objects.filter(wishuser=user_ins)
    context={
        'wishdetail':wishdetail,
    }

    return render(request,'wishlist.html',context)
def wishlistview(request):
    user_ins=request.user
    product_id=request.GET.get('pid')
    productname= productitems.objects.get(id=product_id).productname
    productprice= productitems.objects.get(id=product_id).productprice
    productstatus= productitems.objects.get(id=product_id).productstock
    wishadd= wishitems(wishuser = user_ins,productwish =productname,wishprice=productprice,wishstatus=productstatus,wishid=product_id)
    wishadd.save()
    return redirect('viewwish')

def deletewish(request,wishid):
    delwish=wishitems.objects.filter(wishid=wishid)
    delwish.delete()
    return redirect('viewwish')

def sign_up_view(request):
    if request.method == "POST":
        username = request.POST.get('uname')
        email = request.POST.get('uemail')
        password = request.POST.get('upassword')
        re_password = request.POST.get('re_password')
        if username and email and password and re_password:
            if User.objects.filter(email=email).exists():
                messages.error(request,"Email aready registered. Please use different email or login.")
                return redirect('registration')
            if User.objects.filter(username=username).exists():
                messages.error(request,"Username aready registered. Please use different username or login.")
                return redirect('registration')
            if password==re_password:
                new_user = User.objects.create_user(username, email, password)
                new_user.save()
                user = authenticate(request,username=username, password=password)
                login(request,user)
                user_ins=request.user
                gtotals=0
                cartdototal=carttotal(cartuser=user_ins,gtotal=gtotals)
                cartdototal.save()
                return redirect('home')
            else:
                messages.error(request,"Passwords do not match.")
                return redirect('registration')
        else:
            messages.error(request,"All fields are required.")
            return redirect('registration')
    return render(request,'register.html')


def login_check(request):
    username=request.POST.get('lname')
    login_password=request.POST.get('lpass')
    user = authenticate(request,username=username, password=login_password)
    if request.method == "POST":
        if user is not None:
            login(request,user)
            user_ins=request.user
            gtotals=0
            cartdototal=carttotal(cartuser=user_ins,gtotal=gtotals)
            cartdototal.save()
            return redirect('home')
        else:
            messages.error(request,"Invalid username or password")
            return redirect('/signup_or_signin')
    return render(request,'login-register.html')

import math, random

def forgetpassword(request):
    if request.method == "POST":
        email = request.POST['email']
        digits = "0123456789"
        OTP = ""
        
        
        if User.objects.filter(email = email).exists():
            try:
                for i in range(4):
                    OTP += digits[math.floor(random.random() * 10)]
                send_email = EmailMessage(
                    'Password Change',
                    f'Hello This is your code for password change  {OTP} . Thank You',
                    'eshop5692@gmail.com',
                    [email]
                )
                send_email.send()
                data = ChangePassword.objects.create(email = email,code = OTP)
                data.save()
                return redirect(f'change_password/{email}')
        
            except:
                pass
        else:
            messages.error(request,"The email is not in our list")
            return redirect('/forgetpassword')

    return render(request,'forget_password.html')


def changepassword(request,email):
    if request.method == "POST":
        codes = request.POST['codes']
        password = request.POST['password']
        cpassword = request.POST['cpassword']
        codes =  request.POST['codes']
        username = User.objects.get(email = email).username
        code = ChangePassword.objects.filter(email = email).order_by('-id')[0].code
        if (password == cpassword):
            # try:
            if code == codes:
                data = User.objects.get(email = email)
                data.set_password(password)
                data.save()
                User.objects.filter(email = email).update(username = username)
                messages.error(request,"The password is changed")
                return redirect('/signup_or_signin')
            # except:
            #     pass
    return render(request,'change_password.html')

def logout_task(request):
    logout(request)
    return redirect('home')

def cartviewbasic(request):
    user_ins=request.user.username
    cartdetail=cartitem.objects.filter(cartuser=user_ins)
    # grtotal=carttotal.objects.get(cartuser=user_ins).gtotal
    gtotals=0
    cartusertotal=cartitem.objects.filter(cartuser=user_ins,checkout = False)
    for nums in cartusertotal:
        gtotals=gtotals+nums.subtotal
    context={
        'cartdetail':cartdetail,
        'grtotal':gtotals
    }
    return render(request,'cart.html',context)

def cartview(request):
    product_id=request.GET.get('pid')
    
    productname= productitems.objects.get(id=product_id).productname
    productprice= productitems.objects.get(id=product_id).productprice
    vendor_name = productitems.objects.get(id=product_id).vendor_name
    user_ins=request.user.username
    if  cartitem.objects.filter(cartuser = user_ins,productid =product_id,checkout = False).exists():
        qty = cartitem.objects.get(cartuser=user_ins, productcart=productname, cartprice=productprice,checkout = False).quantity
        print(qty)
        qty = qty+1
        print(qty)
        subtotal = qty*productprice
        cartitem.objects.filter(cartuser = user_ins,productcart =productname,cartprice=productprice).update(subtotal = subtotal,quantity = qty)
    else:
        qty = cartitem(cartuser=user_ins, checkout=False).quantity
        subtotal = qty * productprice
        cartadd = cartitem.objects.create(productid = product_id,cartuser=user_ins, productcart=productname, cartprice=productprice,subtotal = subtotal,vendor_name = vendor_name)
        cartadd.save()
        print("Success")
    return redirect('viewcart')


def deletecartview(request):
    product_id=request.GET.get('pid')
    
    productname= productitems.objects.get(id=product_id).productname
    productprice= productitems.objects.get(id=product_id).productprice
    vendor_name = productitems.objects.get(id=product_id).vendor_name
    user_ins=request.user.username
    if  cartitem.objects.filter(cartuser = user_ins,productid =product_id,checkout = False).exists():

        qty = cartitem.objects.get(cartuser=user_ins, productcart=productname, cartprice=productprice,checkout = False).quantity
        
        if qty>1:
            qty = qty-1
            print(qty)
            subtotal = qty*productprice
            cartitem.objects.filter(cartuser = user_ins,productcart =productname,cartprice=productprice).update(subtotal = subtotal,quantity = qty)
    
    return redirect('viewcart')

def updatecart(request):
    user_ins=request.user
    cartquantity=request.POST.get('productcartquantity')
    cartitemprice=request.POST.get('productcartprice')
    cartitemname=request.POST.get('productcartname')
    cartquantity=int(cartquantity)
    product_id=request.POST.get('cartid')
    updatecartquantity = cartitem.objects.get(id=product_id)
    updatecartquantity.quantity=cartquantity
    updatecartquantity.save()
     #calculate total for confirmation
    gtotals=0
    cartusertotal=cartitem.objects.filter(cartuser=user_ins)
    for nums in cartusertotal:
        gtotals=gtotals+nums.subtotal
    cartdototal=carttotal.objects.get(cartuser=user_ins)
    cartdototal.gtotal=gtotals
    cartdototal.save()
    #..........
    return redirect('viewcart')

def deletecart(request, cartitems_id):
    user_ins=request.user
     #update grandtotal
    # priceofitem = cartitem.objects.get(id=cartitems_id).subtotal
    # cartdototal=carttotal.objects.get(cartuser=user_ins)
    # cartdototal.gtotal=cartdototal.gtotal-priceofitem
    # cartdototal.save()
    #..........
    delcart = cartitem.objects.get(id=cartitems_id)
    delcart.delete()
    return redirect('viewcart')

def checkoutview(request):
    user_ins=request.user
    # if checkout.objects.filter(checkoutuser=user_ins).exists():
    #     # messages.error(request,"You already have a checkout pending. Please wait for your order to complete first.")
    #     # return redirect('viewcart')
    #     pass
    # else:
    #calculate total for confirmation
    gtotals=0
    cartusertotal=cartitem.objects.filter(cartuser=user_ins)
    for nums in cartusertotal:
        gtotals=gtotals+nums.subtotal
    # cartdototal=carttotal.objects.get(cartuser=user_ins)
    # cartdototal.gtotal=gtotals
    # cartdototal.save()
    # carttotaldetail=carttotal.objects.get(cartuser=user_ins).gtotal
    context={
        'carttotaldetail':gtotals
    }
    return render(request,'checkout.html',context)

def docheckout(request):
    #user
    user_ins=request.user
    gtotals = 0
    cartusertotal = cartitem.objects.filter(cartuser=user_ins)
    for nums in cartusertotal:
        gtotals = gtotals + nums.subtotal
    #form data
    Firstname=request.POST.get('c_fname')
    Lastname=request.POST.get('c_lname')
    Email=request.POST.get('c_email')
    Contact=request.POST.get('c_contact')
    msg=request.POST.get('c_msg')
    Address=request.POST.get('c_address')
    #........................
    #cartdetail
    usercartitem=cartitem.objects.filter(cartuser=user_ins,checkout = False)
    if Firstname and Lastname and Email and Contact and msg and Address:
        for item in usercartitem:
            order = f"{item.productcart} QTY -{item.quantity}\n"
            total = item.cartprice * item.quantity

            checkoutsave = vendorcheckout.objects.create(firstname = Firstname,lastname = Lastname,contactnumber = Contact,message=msg,address = Address,email = Email,total=total,checkoutuser=user_ins,orderinfo=order,vendor= item.vendor_name)
            checkoutsave.save()
    else:
        messages.error(request,"Please fill all the fields.")
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    # products=[]
    # quantity=[]
    # userorderinfo={}
    # for item in usercartitem:
    #     products.append(item.productcart)
    #     quantity.append(item.quantity)
    # # print(products)
    # # print(quantity)
    # userorderinfo=dict(zip(products,quantity))
    # print(userorderinfo)
    userorderinfo = ""
    for item in usercartitem:
        order = f"{item.productcart} QTY -{item.quantity}\n"
        userorderinfo = userorderinfo+order

        checkoutsave = checkout.objects.create(firstname = Firstname,lastname = Lastname,contactnumber = Contact,message=msg,address = Address,email = Email,total=gtotals,checkoutuser=user_ins,orderinfo=userorderinfo)
        checkoutsave.save()
    checkoutsave.save()
    delcart = cartitem.objects.filter(cartuser=user_ins)
    delcart.delete()
    try:
        #reset grandtotal
        cartdototal=carttotal.objects.get(cartuser=user_ins)
        cartdototal.gtotal=0
        cartdototal.save()
        #..........
        cartitem.objects.filter(cartuser=user_ins, checkout=False).update(checkout = True)
    except:
      print('An exception occurred')
    messages.success(request,"Your order has been placed. Please wait for your order to be processed.")
    return redirect('/myorder')

def vieworders(request):
    # try:
    user_ins=request.user
    userorder=vendorcheckout.objects.filter(checkoutuser=user_ins)
    user_ins=request.user
    # if checkout.objects.filter(checkoutuser=user_ins).exists():
    #     # messages.error(request,"You already have a checkout pending. Please wait for your order to complete first.")
    #     # return redirect('viewcart')
    #     pass
    # else:
    #calculate total for confirmation
    gtotals=0
    cartusertotal=cartitem.objects.filter(cartuser=user_ins)
    for nums in cartusertotal:
        gtotals=gtotals+nums.subtotal
    # cartdototal=carttotal.objects.get(cartuser=user_ins)
    # cartdototal.gtotal=gtotals
    # cartdototal.save()
    # carttotaldetail=carttotal.objects.get(cartuser=user_ins).gtotal
    context={
        'carttotaldetail':gtotals,
        'userorder':userorder
    }
    # usertotal=checkout.objects.get(checkoutuser=user_ins,status = 'follow').total
    # products=[]
    # quantitys=[]
    # for i in order:
    #     userorder=i.orderinfo
    # userorder = checkout.objects.filter()
    # userorder=yaml.load(userorder, Loader=yaml.FullLoader)
    # userorder=userorder.objects.all()[-1:]
    return render(request,'myorders.html',context)
    # except:
    # messages.error(request,"You don't have any orders.")
    # return redirect('viewcart')

def sendemail(emailtemplate,username,toemail):
    from mail_templated import EmailMessage
    message = EmailMessage(emailtemplate, {'user': username},'eshop5692@gmail.com.com',to=[toemail])
    message.send()
