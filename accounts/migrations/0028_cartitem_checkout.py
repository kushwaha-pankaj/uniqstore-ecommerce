# Generated by Django 3.0.2 on 2021-04-15 05:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0027_auto_20210415_1020'),
    ]

    operations = [
        migrations.AddField(
            model_name='cartitem',
            name='checkout',
            field=models.BooleanField(default=False),
        ),
    ]
