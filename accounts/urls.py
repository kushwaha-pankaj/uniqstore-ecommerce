from django.urls import path
from . import views
from vendor.views import update_order_status
urlpatterns = [
    path('signup/',views.sign_up_view,name='registration'),
    path('forgetpassword',views.forgetpassword,name='forgetpassword'),
    path('change_password/<str:email>',views.changepassword,name='change_password'),
    path('signup_or_signin/',views.login_check,name='signup_or_signin'),

    path('logout',views.logout_task),
    # path('home/logout',views.logout_task),

    path('wishlist',views.wishlistview),
    path('cart',views.cartview),
    path('viewcart',views.cartviewbasic,name='viewcart'),
    path('deletecartview',views.deletecartview,name='deletecartview'),
    # path('home/viewcart',views.cartviewbasic),
    path('update',views.updatecart),
    path("delete/<int:cartitems_id>/", views.deletecart),
    path("deletewish/<int:wishid>/", views.deletewish),
    # path('grandtotal',views.dototal),
    path('checkoutview',views.checkoutview),
    path('checkout',views.docheckout),

    path('myorder',views.vieworders),
    path('update_order_status/<int:id>',update_order_status,name='update_order_status'),
    # path('home/myorder',views.vieworders),
    
    path('viewwish',views.wishviewbasic,name='viewwish')
]


