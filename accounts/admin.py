from django.contrib import admin
from . models import cartitem,carttotal,checkout,wishitems,vendorcheckout,ChangePassword
# Register your models here.

class item(admin.ModelAdmin):
    list_display = ("firstname","contactnumber","email","status","orderinfo","vendor")
    search_fields = ["firstname","contactnumber","email","vendor"]
    list_filter = ("status","date","vendor")
    list_per_page = 20
    
    def has_add_permission(self,request):
        return True

admin.site.register(checkout,item)

class items(admin.ModelAdmin):
    list_display = ("firstname","contactnumber","email","status","orderinfo","vendor")
    search_fields = ["firstname","contactnumber","email","vendor","orderinfo"]
    list_filter = ("status","vendor","date")
    list_per_page = 20
    
    def has_add_permission(self,request):
        return True

admin.site.register(vendorcheckout,item)
admin.site.register(wishitems)
admin.site.register(cartitem)

# admin.site.register(carttotal)
admin.site.register(ChangePassword)

