from django.db import models
from products.models import productitems
from django.contrib.auth.models import User
from datetime import datetime
from products.models import productitems

class ChangePassword(models.Model):
    email = models.CharField(max_length = 300)
    code = models.CharField(blank = True,max_length = 100)
    def __str__(self):
        return self.email

class cartitem(models.Model):
    productid = models.IntegerField(default = 0)
    productcart=models.CharField(max_length=200,default='')
    quantity=models.IntegerField(default=1)
    cartprice=models.FloatField(blank=True)
    cartuser=models.CharField(blank = True,max_length = 300)
    subtotal=models.FloatField(blank=True,default=cartprice)
    checkout = models.BooleanField(default = False)
    vendor_name = models.CharField(max_length = 500,blank = True)
    class Meta:
        verbose_name = 'Cart Item'
        verbose_name_plural = 'Cart Item'
    def save(self, *args, **kwargs):
        self.subtotal=self.cartprice*self.quantity
        super().save(*args, **kwargs)
    def __str__(self):
        return self.productcart
        
class carttotal(models.Model):
    cartuser=models.CharField(blank = True,max_length=300)
    gtotal=models.FloatField(blank=True,default=0)
    def __str__(self):
        return self.cartuser

class checkout(models.Model):
    firstname=models.CharField(max_length=100, blank=False)
    lastname=models.CharField(max_length=100, blank=False)
    contactnumber=models.CharField(max_length=10, blank=False)
    message=models.TextField(max_length=1500, blank=True)
    address=models.CharField(max_length=100, blank=False)
    email=models.EmailField(max_length=250, blank=False)
    total=models.FloatField(blank=True,default=0)
    checkoutuser=models.ForeignKey(User,on_delete=models.CASCADE)
    vendor = models.CharField(max_length = 500,blank = True)
    orderinfo=models.TextField(max_length=1000,blank=True,default='')
    date =models.DateTimeField(auto_now=True,null = True)
    status = models.CharField(max_length = 200,default = 'received')
    class Meta:
        verbose_name = 'Checkout'
        verbose_name_plural = 'Checkout List'
    def __str__(self):
        return self.firstname



class vendorcheckout(models.Model):
    firstname=models.CharField(max_length=100, blank=False)
    lastname=models.CharField(max_length=100, blank=False)
    contactnumber=models.CharField(max_length=10, blank=False)
    message=models.TextField(max_length=1500, blank=True)
    address=models.CharField(max_length=100, blank=False)
    email=models.EmailField(max_length=250, blank=False)
    total=models.FloatField(blank=True,default=0)
    checkoutuser=models.ForeignKey(User,on_delete=models.CASCADE)
    vendor = models.CharField(max_length = 500,blank = True)
    orderinfo=models.TextField(max_length=1000,blank=True,default='')
    date =models.DateTimeField(auto_now=True,null = True)
    status = models.CharField(max_length = 200,default = 'unknown')
    class Meta:
        verbose_name = 'Vendor Checkout'
        verbose_name_plural = 'Vendor Checkout List'

    def __str__(self):
        return self.firstname


class wishitems(models.Model):
    productwish=models.CharField(max_length=200,default='')
    wishprice=models.FloatField(blank=True)
    wishuser=models.CharField(blank = True,max_length=300)
    wishstatus=models.CharField(max_length=200,default='')
    wishid=models.IntegerField(blank=False,default=0)
    class Meta:
        verbose_name = 'Wish List'
        verbose_name_plural = 'Wish List'

    def __str__(self):
        return self.productwish
