from django.contrib import messages
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate,login,logout
# Create your views here.
from django.views.generic.base import View
from home.models import *
from products.models import *
from accounts.models import *
from vendor.models import *
from django.core.files.storage import FileSystemStorage
from django.core.mail import EmailMessage
from django.contrib.auth.decorators import login_required



class BaseView(View):

    template_context = {}


class DashBoard(BaseView):
    def get(self, request):
        if request.user.username:
            pass
        else:
            redirect('/')
        user_name = request.user.username
        vendors = Vendor.objects.get(user = user_name).name
        self.template_context = {}

        self.template_context['no_product'] = productitems.objects.filter(vendor_name = vendors).count()
        self.template_context['no_sales'] = vendorcheckout.objects.filter(vendor = vendors).count()
        return render(request, 'target-free-admin-template/index.html',self.template_context)

class Products(BaseView):
    def get(self, request):
        if request.user.username:
            pass
        else:
            redirect('/')
        user = request.user.username
        vender_name = Vendor.objects.get(user = user).name
        self.template_context['products'] =  productitems.objects.filter(vendor_name = vender_name)
        return render(request, 'target-free-admin-template/product.html',self.template_context)

class SearchProducts(BaseView):
    def get(self, request):
        if request.user.username:
            pass
        else:
            redirect('/')
        user = request.user.username
        vendor_name = Vendor.objects.get(user = user).name
        query = request.GET.get('query',None)
        
        self.template_context['search_query']=productitems.objects.filter(productname__icontains = query,vendor_name = vendor_name)
        return render(request, 'target-free-admin-template/search_product.html',self.template_context)

class Order(BaseView):
    def get(self, request):
        if request.user.username:
            pass
        else:
            redirect('/')
        user_name = request.user.username
        vendors = Vendor.objects.get(user = user_name).name
        self.template_context['orders'] = vendorcheckout.objects.filter(vendor = vendors)
        return render(request, 'target-free-admin-template/order.html',self.template_context)

class FilterOrder(BaseView):
            
     def get(self, request):
        if request.user.username:
            pass
        else:
            redirect('/')
        user_name = request.user.username
        vendors = Vendor.objects.get(user = user_name).name
        query = request.GET.get('query',None)
        self.template_context['filter_orders'] = vendorcheckout.objects.filter(status = query,vendor = vendors)
        return render(request, 'target-free-admin-template/filter_object.html',self.template_context)


class AddProduct(BaseView):
   
    def get(self, request):
        if request.user.username:
            pass
        else:
            redirect('/')
        self.template_context['categories'] = producttype.objects.all()
        return render(request, 'target-free-admin-template/add_product.html',self.template_context)

class AddImage(BaseView):
    def get(self, request):
        if request.user.username:
            pass
        else:
            redirect('/')
        self.productid = request.GET.get('pid')
        self.template_context['productid'] = self.productid
        self.template_context['productid_image'] = productimages.objects.filter(product_id = self.productid)
        self.template_context['prductimage'] = productitems.objects.filter(id = self.productid)
        return render(request, 'target-free-admin-template/add_image.html',self.template_context)


def add_image(request):
   
    if request.method == 'POST':
        image1 = request.FILES.get('image')
        productid = request.POST.get('product_id')
    
    
        data = productimages.objects.create(product_id = productid,image = image1)
        data.save()
        # fss = FileSystemStorage()
        # img = fss.save(request.FILES['image'].name,request.FILES['image'])
        # file_url1 = fss.url(img)
        
        messages.success(request, "The Image is Updated successfully ")
    return redirect(f'/vendor/add_image?pid={productid}')

def delete_image(request):
    productid = request.GET.get('pid')
    imageid = request.GET.get('imid')
    productimages.objects.filter(id = imageid).delete()
    messages.success(request, "The Image is deleted successfully ")
    return redirect(f'/vendor/add_image?pid={productid}')


class EditProduct(BaseView):
   
    def get(self, request):
        if request.user.username:
            pass
        else:
            redirect('/')
        self.productid = request.GET.get('pid')
        self.template_context['edit_product'] = productitems.objects.filter(id=self.productid)
        self.template_context['cat_product'] = producttype.objects.filter(status = "active")

        return render(request, 'target-free-admin-template/edit_product.html',self.template_context)


def addproduct(request):
    cate = producttype.objects.filter(status = "active")
    if request.method == 'POST':
        username = request.user.username
        vendors = Vendor.objects.get(user = username).name
        productname = request.POST['productname']
        productprice = request.POST['productprice']
        productimg = request.FILES['productimg']
        description = request.POST['description']
        vendor_user_name = request.POST['vendor_user_name']
        category =  request.POST.get('category')
        if productname and productprice and productimg and description and vendor_user_name and cate:
            product = productitems.objects.create(productname = productname,
                                                vendor_name =vendors,
                                                productprice = productprice,
                                                productimg = productimg,
                                                productdescription = description,
                                                categorytype_id = category
                                                )
            product.save()
            messages.success(request, "The product is added successfully ")
        else:
            messages.error(request, "Please fill all the fields ")
    context ={
        'categories':cate,
    }
    return render(request, 'target-free-admin-template/add_product.html',context)

def delete_product(request):
    productid = request.GET.get('pid')
    productitems.objects.filter(id = productid).delete()
    messages.success(request, "The product is successfully removed.")
    return redirect('vendor:product')

def edit_product(request):
    if request.method == 'POST':
        id = request.POST['id']
        productname = request.POST['productname']
        productprice = request.POST['productprice']
        category =  request.POST['category']
        cate = producttype.objects.filter(id = category)[0]
        change_img = request.POST.get('change_img', None)
        print(type(change_img))
        description = request.POST['description']
        if change_img != "on":
            productitems.objects.filter(id = id).update(productname = productname,
                                                  productprice = productprice,
                                                  productdescription = description,
                                                   categorytype = cate
                                                  )

        else:

            productitems.objects.filter(id=id).update(productname=productname,
                                                      productimg=request.FILES['productimg'],
                                                      productprice=productprice,
                                                      productdescription=description,
                                                       categorytype = cate
                                                      )
            fss = FileSystemStorage()
            file = fss.save(request.FILES['productimg'].name,request.FILES['productimg'])
            file_url = fss.url(file)
            messages.success(request, "The product is Updated successfully ")
    return redirect('vendor:product')

def login_check(request):
    def get(self, request):
        if request.user.username:
            redirect('/')
        else:
            pass
    if request.method == 'POST':
        username=request.POST.get('username')
        login_password=request.POST.get('password')
        if  Vendor.objects.filter(verify='yes',user = username).exists():
            if Vendor.objects.filter(user= username).exists():
                user = authenticate(request,username=username, password=login_password)

                if user:
                    login(request,user)
                    return redirect('/vendor/dashboard')
                else:
                    messages.error(request,"Username or Password do not match.")
                    return redirect('/vendor/login')

            else:
                messages.error(request, "There is some error.")
                return redirect('/vendor/login')
        else:
            messages.error(request, "You are not verified or not an user")
            return redirect('/vendor/login')

    return render(request,'target-free-admin-template/form.html')

def vendorsignup(request):
    if request.user.username:
            redirect('/')
    else:
        pass
    if request.method =='POST':
        first_name = request.POST['first_name']
        last_name = request.POST['last_name']
        username = request.POST['username']
        email = request.POST['email']
        vendor_name = request.POST['vendor_name']
        phone = request.POST['phone']
        password = request.POST['password']
        cpassword = request.POST['cpassword']
        msg = {}
        if first_name and last_name and username and email and vendor_name and phone and password and cpassword:
            if password == cpassword:

                if User.objects.filter(username =username).exists():
                    messages.error(request,"The username is already taken")
                    error = "The username is already taken"
                    context = {
                        'first_name':first_name,
                        'last_name':last_name,
                        'username':username,
                        'email':email,
                        'vendor_name':vendor_name,
                        'phone':phone,
                        'error':error
                    }
                    return render(request,'target-free-admin-template/vendorsignup.html',context)

                elif User.objects.filter(email =email).exists():
                    messages.warning(request,"The email is already taken")
                    warning = "The email is already taken"
                    context = {
                        'first_name':first_name,
                        'last_name':last_name,
                        'username':username,
                        'email':email,
                        'vendor_name':vendor_name,
                        'phone':phone,
                        'warning':warning
                    }
                    return render(request,'target-free-admin-template/vendorsignup.html',context)

                elif Vendor.objects.filter(name =vendor_name).exists():
                    messages.info(request,"The Vendor name is already taken")
                    info = "The Vendor name is already taken"
                    context = {
                        'first_name':first_name,
                        'last_name':last_name,
                        'username':username,
                        'email':email,
                        'vendor_name':vendor_name,
                        'phone':phone,
                        'info':info
                    }
                    return render(request,'target-free-admin-template/vendorsignup.html',context)

                else:
                    user = User.objects.create_user(
                        first_name = first_name,
                        last_name = last_name,
                        username = username,
                        email = email,
                        password = password
                    )
                    user.save()
                    vendors = Vendor.objects.create(
                        user = username,
                        contact = phone,
                        name = vendor_name,
                        slug = vendor_name,
                        email = email
                        )
                    vendors.save()
                    return redirect('/vendor/login')
            else:
                messages.success(request, "The passwords do not match ")
                success = "The passwords do not match"
                context = {
                    'first_name':first_name,
                    'last_name':last_name,
                    'username':username,
                    'email':email,
                    'vendor_name':vendor_name,
                    'phone':phone,
                    'success':success
                }
                return render(request,'target-free-admin-template/vendorsignup.html',context)
        else:
            messages.error(request, "Please fill all the fields")
    return render(request,'target-free-admin-template/vendorsignup.html')

def update_order_status(request,id):
    if request.method == "POST":
        action = request.POST['action']
        vendorcheckout.objects.filter(id = id).update(status = action)
        email = vendorcheckout.objects.get(id = id).email
        firstname = vendorcheckout.objects.get(id = id).firstname
        lastname = vendorcheckout.objects.get(id = id).lastname
        orderinfo = vendorcheckout.objects.get(id = id).orderinfo
        try:
            send_email = EmailMessage(
                'Contact from your store',
                f'Hello {firstname} {lastname} your order {orderinfo} is in {action}. Thank You',
                'eshop5692@gmail.com',
                [email]
            )
            send_email.send()
        except:
            pass
        
        messages.success(request,f'Status Updated to {action}')
        return redirect('/vendor/order')


