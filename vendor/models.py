from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.conf import settings
from django.core.mail import EmailMessage


VERIFY = (('yes','yes'),('no','no'))
# Create your models here.
class District(models.Model):
    name = models.CharField(max_length=300)
    slug = models.CharField(max_length=300)
    food = models.BooleanField(default=False)

    def __str__(self):
        return self.name

class Vendor(models.Model):
    user = models.CharField(max_length=300)
    name = models.CharField(max_length = 500)
    email = models.CharField(max_length = 500,null = True)
    slug = models.CharField(max_length=20,unique=True)
    district = models.ForeignKey(District, on_delete=models.CASCADE,null = True)
    contact = models.TextField(null=True)
    verify = models.CharField(choices = VERIFY,max_length=50,blank = True)
    confirm_email = models.BooleanField(default = False)

    def __str__(self):
        return self.name

@receiver(post_save, sender=Vendor)
def send_new_notification_email(sender, instance, created, **kwargs):
    # if a new vedor is created, compose and send the email
    if instance.verify == 'yes':
        mail_subject = 'You have verified'
        message = 'You have been verified as a vendor. Please login to your account to view your profile.'
        send_email = EmailMessage(mail_subject, message, to=[instance.email,], from_email=settings.EMAIL_HOST_USER)
        send_email.send()
            