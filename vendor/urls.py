from django.urls import path

from .views import *
app_name = 'vendor'

urlpatterns = [
    path('login/', login_check,name = 'login'),
    path('signup/', vendorsignup,name = 'signup'),
    path('dashboard',DashBoard.as_view(),name = 'vendor'),
    path('order',Order.as_view(),name = 'order'),
    path('product',Products.as_view(),name = 'product'),
    path('add_product',AddProduct.as_view(),name = 'add_product'),
    path('add_image',AddImage.as_view(),name = 'add_image'),
    path('add_images',add_image,name = 'add_images'),
    path('delete_image',delete_image,name = 'delete_image'),
    path('product_add',addproduct,name = 'product_add'),
    path('edit_product',EditProduct.as_view(),name = 'edit_product'),
    path('delete_product',delete_product,name = 'delete_product'),
    path('product_edit',edit_product,name = 'product_edit'),
    path('product_search',SearchProducts.as_view(),name = 'product_search'),
    path('filterorder',FilterOrder.as_view(),name = 'filterorder'),
]