from django.contrib import admin

# Register your models here.
from django.contrib import admin
from . models import *
# Register your models here.
class vendors(admin.ModelAdmin):
    readonly_fields = ("user","name","email","contact","slug",)
    list_display = ("user","name","email","verify","contact")
    search_fields = ["name","contact","email","verify"]
    list_filter = ("verify","confirm_email")
    list_per_page = 20
    
    def has_add_permission(self,request):
        return True
    

admin.site.register(Vendor,vendors)
admin.site.register(District)
# admin.site.register(Vendor)

