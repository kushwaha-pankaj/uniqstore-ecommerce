# Generated by Django 3.0.2 on 2021-09-25 13:35

import ckeditor.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0007_auto_20210925_1845'),
    ]

    operations = [
        migrations.AlterField(
            model_name='productitems',
            name='productdescription',
            field=ckeditor.fields.RichTextField(blank=True),
        ),
        migrations.AlterField(
            model_name='productreview',
            name='review',
            field=ckeditor.fields.RichTextField(blank=True),
        ),
    ]
