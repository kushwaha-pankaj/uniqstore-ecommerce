from django.contrib import admin
from . models import producttype,productitems,productreview,productimages
# Register your models here.
class item(admin.ModelAdmin):
    list_display = ("productname","productprice","categorytype","vendor_name","status")
    search_fields = ["productname","productdescription","vendor_name"]
    list_filter = ("vendor_name","categorytype","status")
    list_per_page = 20
    readonly_fields = ("vendor_name",)
    
    def has_add_permission(self,request):
        return True


admin.site.register(productitems,item)

admin.site.register(producttype)

admin.site.register(productreview)
admin.site.register(productimages)