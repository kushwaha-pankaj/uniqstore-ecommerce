from django.shortcuts import render,redirect
from products.models import producttype,productitems,productreview,productimages
from accounts.models import cartitem
from . models import blog,blogcomment,Contact
from vendor.models import *
from urllib.parse import urlencode
from django.urls import reverse
from rest_framework import routers, serializers, viewsets
from django.contrib.auth.models import User
from . serializers import UserSerializer
from django.core.mail import EmailMessage
from django.contrib import messages


# Create your views here.
def indexview(request):
    products=productitems.objects.filter(status = "active")
    hot=productitems.objects.filter(status = "active",labels='special')[:3]
    new = productitems.objects.filter(status = "active",labels='new')[:3]
    top = productitems.objects.filter(status = "active",labels='top')[:3]
    types=producttype.objects.filter(status = "active")
    no_vendor = Vendor.objects.filter(verify = "yes",confirm_email = False).count()
    productsimages = productimages.objects.all()
    
    if Vendor.objects.filter(verify = "yes",confirm_email = False).exists():
        for i in Vendor.objects.filter(verify = "yes",confirm_email = False):
            # data = Vendor.objects.filter(verify = "yes",confirm_email = False)[i]
            # # email = data['email']
            # print(data)
            email = i.email
            vendor_name = i.name
            try:
                send_email = EmailMessage(
                    'Vendor Confirmation',
                    f'Hello {vendor_name} your request for being Vendor is approved. Thank You',
                    'eshop5692@gmail.com',
                    [email]
                )
                send_email.send()
                Vendor.objects.filter(verify = "yes",confirm_email = False).update(confirm_email = True)
            except:
                pass

    context={
        'products':products,
        'types':types,
        'hot':hot,
        'new':new,
        'top':top,
        'no_vendor':no_vendor,
        'productsimages':productsimages
    }
    return render(request,'index.html',context)

def aboutview(request):
    return render(request,'about.html')

def blogview(request):
    blogs=blog.objects.all()
    context={
        'blogs':blogs
    }
    return render(request,'blog.html',context)

def blogdetails(request):
    blogid=request.GET.get('bid')
    blogs=blog.objects.filter(id=blogid)
    latestblog = blog.objects.order_by('-postdate')
    comments=blogcomment.objects.filter(blogpostid=blogid).order_by('-commentdate')
    context={
        'blogs':blogs,
        'comments':comments,
        'latestblog':latestblog
    }
    return render(request,'blog-details.html',context)
def commentpost(request):
    user_ins=request.user
    blogid=request.POST.get('blogid')
    commentpost=request.POST.get('ucomment')
    commentsave=blogcomment(commenter=user_ins,comment=commentpost,blogpostid=blogid)
    commentsave.save()
     #for url redirect
    base_url = reverse('blogshow')
    query_string =  urlencode({'bid': blogid})
    url = '{}?{}'.format(base_url, query_string)
    return redirect(url) 

    
def shopview(request):
    products=productitems.objects.order_by('?')
    types=producttype.objects.all()

    context={
        'products':products,
        'types':types
    }
    return render(request,'shop.html',context)


def searchview(request):
    if request.user.username:
            pass
    else:
        redirect('/')
    query = request.GET.get('query',None)
    template_context = {}
    template_context['search_query']=productitems.objects.filter(productname__icontains = query,status = 'active')
    return render(request, 'search.html',template_context)

def contactview(request):
    msg = ''
    if request.method =='POST':
        name = request.POST['name']
        email = request.POST['email']
        subject = request.POST['subject']
        message = request.POST['message']

        if name and email and subject and message:
            data = Contact.objects.create(
            name= name,
            email = email,
            message = message,
            subject = subject
            )
            data.save()
            successmsg = 'Your message has been sent successfully'
        else:
            return redirect('contact')
    else:
        msg = 'Please fill the form'
        successmsg = ''
    context = {
        'msg':msg,
        'successmsg':successmsg
    }
    return render(request,'contact.html',context)

def productdetailview(request):
    user_ins=request.user
    productid=request.GET.get('pid')
    productinfo=productitems.objects.filter(id=productid)
    productimage = productimages.objects.filter(product_id=productid)
    print(productimage)
    reviews=productreview.objects.filter(reviewid=productid).order_by('-reviewdate')

    context={
        'productinfo':productinfo,
        'reviews':reviews,
        'productimage':productimage
    }
    return render(request,'product-details.html',context)

def reviewsave(request):
    user_ins=request.user
    rid=request.POST.get('pid')
    reviewpost=request.POST.get('review')
    reviewsave=productreview(reviewer=user_ins,review=reviewpost,reviewid=rid)
    reviewsave.save()
    #for url redirect
    base_url = reverse('productdetail')
    query_string =  urlencode({'pid': rid})
    url = '{}?{}'.format(base_url, query_string)
    return redirect(url) 


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
