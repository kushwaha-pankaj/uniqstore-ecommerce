from django.urls import path
from . import views
urlpatterns = [
    path('',views.indexview,name='home'),
    
    path('blog',views.blogview,name='blog'),
    path('blog-details',views.blogdetails,name='blogshow'),
    path('shop',views.shopview,name='shop'),
    # path('home/shop',views.shopview),
    path('contact',views.contactview, name='contact'),
    path('productdetail',views.productdetailview),
    path('productdetail',views.productdetailview,name='productdetail'),
    path('postcomment',views.commentpost),
    # path('home/sendreview',views.reviewsave),
    path('sendreview',views.reviewsave),
    path('searchview',views.searchview),
    
]