from django.contrib import admin
from . models import blog,blogcomment,Contact
# Register your models here.

admin.site.site_title = "My Shop"
admin.site.site_header = "My Shop"
class blogadmin(admin.ModelAdmin):
    list_display=('blogwriter','blogtitle','postdate')
admin.site.register(blog,blogadmin)
admin.site.register(blogcomment)
admin.site.register(Contact)